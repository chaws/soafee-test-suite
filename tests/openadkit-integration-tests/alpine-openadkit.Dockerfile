FROM alpine:3.17.1

RUN apk update && \
    apk add iproute2-minimal=6.0.0-r1 && \
    apk add iputils=20211215-r0
