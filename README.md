# SOAFEE Test Suite

SOAFEE compliance test to determine whether a software stack is SOAFEE compliant
or not.

## Installation

`soafee-test-suite` is a bash script and is self-contained with only awk as
dependency. It will work without installation, but it can be installed.

Install SOAFEE Test Suite:

```bash
make install
```

## Setup SOAFEE Test Suite:

Please, run only once `sudo ./soafee-test-suite-setup` before `soafee-test-suite`.

## Running SOAFEE Test Suite:

To run SOAFEE Test Suite:

```bash
soafee-test-suite run -r
```

`soafee-test-suite run` is a wrapper around the `bats` tool. Any options to
`run` will also be passed to `bats`. Hint: run `soafee-test-suite run -h` to get
`bats` help.

## SOAFEE Test Suite Test Plan

We have ported EWAOL tests and started adding OpenAD Kit tests. Both test
suites shared similar tests that we can reuse by creating test plans.

At the moment we have two test plans: `soafee` and `openadkit`.

To run the test plan filter the tests with `--filter-tags`. Example:

Run only `soafee` test plan:
```bash
soafee-test-suite run -r --filter-tags soafee
```

## SOAFEE Test Suite Usage and Examples

:

## TODO

* ~~Add SOAFEE Test Suite Bitbake recipe~~

* ~~Port all EWAOL tests to SOAFEE~~

* Add SOAFEE Test Suite Usage and Examples

* ~~Remove meta-* after done with them~~

* Update the documentation according to SOAFEE Test Suite

* Remove unusued integration tests from meta-ewaol

## License

[MIT](https://choosealicense.com/licenses/mit/)

### SPDX Identifiers

Individual files contain the following tag instead of the full license text.

```text
  SPDX-License-Identifier: MIT
```

This enables machine processing of license information based on the SPDX
License Identifiers that are here available: http://spdx.org/licenses/
